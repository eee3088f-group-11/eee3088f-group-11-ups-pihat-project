# EEE3088F Group 11 UPS PiHat Project

Design for an Uninterrupted Power Supply (UPS) PiHat for a Raspberry Pi Zero.

The project requires that 3 submodules must be designed and simulated for the PiHat. These 3 submodules being a switching power regulator, 3 or more status LEDs and an amplifier circuit. 

The use cases of the project are:
1) To enable the use of the Raspberry Pi Zero during the length of load shedding.
2) To allow the Raspberry Pi Zero to be portable while in use.
3) As a safety mechanism against power outages so that the Raspberry Pi Zero is not damaged.

## How to Use the UPS uHAT

The uHAT connects to the Pi Zero through the use of the GPIO pins of the Pi Zero. The AC to DC converter of the UPS must be connected to the 12V transformer supply. This is so that the UPS can register that the mains supply has gone down and start supplying the Pi Zero with the use of the battery. The status LEDs will display the battery percentage remaining until the UPS is no longer usable and is a self-regulating circuit. Each LED represents 10% of the full battery percentage. The batteries will need to be recharged separately. Once the UPS is connected to both the Pi Zero and the mains it will automatically switch between Mains Supply Mode and Battery Supply Mode when needed. No manual switching is required.

## Maintenance

Should maintenace need to be performed on the UPS, the button on the UPS can be pressed to stop the UPS from supplying power to the Pi Zero and thus allows for a safe way to disconnect the Pi Zero from the UPS power.
