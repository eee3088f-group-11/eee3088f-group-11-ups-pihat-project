## Active UPS Indicator

The single blue LED should be interpreted as representing whether the UPS system is being used or not. When the LED is OFF it means the system is in Mains Supply Mode and power is being supplied to the Pi Zero through mains. When the LED is ON it means the system is in Battery Supply Mode and power is being supplied to the Pi Zero through the UPS battery.

## Battery Voltage Status Indicator

The remaining ten LEDs should be interpreted as representing the percentage of battery voltage remaining until the UPS becomes unusable. When all the LEDs are ON then the battery is at full voltage of 14.4V. As the battery drains the LEDs will each turn off one by one as 10% of battery is drained by the system. When the battery reaches 6V only one LED will be ON. This means the UPS is close to being unusable and that the user should make sure to shut down the Raspberry Pi Zero as soon as possible.
