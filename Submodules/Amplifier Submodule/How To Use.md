# How To Use:
Step 1: The amplifier ciruit is very simple to use. It requires no input from the user and is already set up with the micro-HAT and it has no external connections for the user to connect to. The HAT is purposefully designed this way to ensure that the amplifying circuit acts as an automatic switch for the HAT. The amplifying circuit will automatically switch to the battery supply source when the mains power drops below the required power of the Pi Zero and it will switch back to using mains power when the mains can supply sufficient power again.

Step 2: The user should not connect any external circuitry to the amplifier submodule, however, should they wish to fault check with a multimeter, they need to press the maintenace switch on the side of the device.

Step 3: Safely disconnect the UPS device from the Pi Zero.

Step 4: Perform maintenace or fault check.

## Maintenance:
This amplifier circuit does allow the user to stop the power supply to the Pi Zero via use of the maintence switch. This maintence switch is added to the circuit to allow the user to stop powering the Pi Zero manually which allows them to disconnect the HAT from the Pi Zero.This will allow the user to investigate and debug if there is a fault with their device and allows for the fixes to be easilly done.
