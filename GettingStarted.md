## Getting Started Using the UPS uHAT for the Pi Zero

The user should start by inserting the 14.4V batteries into the battery holder on the uHAT. The user must then connect the uHAT to the Pi Zero through the 40 Pin GPIO Connector. Once the uHAT is connected to the Pi Zero the UPS will then function automatically as long as the Pi Zero is initially powered by the mains supply. When the battery runs low the user will need to remove them and recharge them manually.

When the mains power goes down the UPS system will register this change and start supplying the Pi Zero through the UPS battery. When mains power comes back online the UPS system will register this change and will stop supplying the Pi Zero will the UPS battery. This switching mechansim is automatic. The user does not need to act for these changes to occur.

There are 10 LEDs on the UPS uHAT that will indicate how much percentage of the battery's voltage remains. These LEDs will keep the user aware of how much longer they are able to use the Pi Zero so that they can shut it down properly before the UPS stops supplying power. The UPS tracks the battery voltage from 14.4V to 6V as the UPS no longer functions once the battery drops below 6V. There is an 11th LED that will indicate to the user when the UPS is in Battery Supply Mode.
