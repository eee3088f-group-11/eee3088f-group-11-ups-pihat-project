### Manufacturing Notes

To manufacture this uHAT the user requires a LM3914 IC for control of the status LEDs and a LTC3895IFE#TRPBF is required for the circuit voltage regulation. 10 LEDs are required for the status interface. These LEDs can be different colours such as green, yellow and red to indicate in what range the current status of the battery is in. The batteries will need to be stacked as a result of the small size of the uHAT. Rechargable batteries are preferable as they can be easily reused. The board has two internal layers. One internal layer is the GND and the other is the battery of the voltage.

#### Bill of Materials:

https://gitlab.com/eee3088f-group-11/eee3088f-group-11-ups-pihat-project/-/blob/main/BOM_project.xlsx
