EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Amplifier for UPS uHAT"
Date "2021-06-01"
Rev "0.1"
Comp "Group 11"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pspice:DIODE D13
U 1 1 606B825A
P 4350 5000
F 0 "D13" V 4396 4872 50  0000 R CNN
F 1 "DIODE" V 4305 4872 50  0000 R CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4350 5000 50  0001 C CNN
F 3 "~" H 4350 5000 50  0001 C CNN
	1    4350 5000
	0    -1   -1   0   
$EndComp
$Comp
L pspice:DIODE D11
U 1 1 606B9802
P 3850 5000
F 0 "D11" V 3896 4872 50  0000 R CNN
F 1 "DIODE" V 3805 4872 50  0000 R CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3850 5000 50  0001 C CNN
F 3 "~" H 3850 5000 50  0001 C CNN
	1    3850 5000
	0    -1   -1   0   
$EndComp
$Comp
L pspice:DIODE D12
U 1 1 606BB9BC
P 3850 5600
F 0 "D12" V 3896 5472 50  0000 R CNN
F 1 "DIODE" V 3805 5472 50  0000 R CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3850 5600 50  0001 C CNN
F 3 "~" H 3850 5600 50  0001 C CNN
	1    3850 5600
	0    -1   -1   0   
$EndComp
$Comp
L pspice:DIODE D14
U 1 1 606BCA86
P 4350 5600
F 0 "D14" V 4396 5472 50  0000 R CNN
F 1 "DIODE" V 4305 5472 50  0000 R CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4350 5600 50  0001 C CNN
F 3 "~" H 4350 5600 50  0001 C CNN
	1    4350 5600
	0    -1   -1   0   
$EndComp
$Comp
L pspice:CAP C3
U 1 1 606BCD9D
P 4800 5250
F 0 "C3" H 4978 5296 50  0000 L CNN
F 1 "470u" H 4978 5205 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 4800 5250 50  0001 C CNN
F 3 "~" H 4800 5250 50  0001 C CNN
	1    4800 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 4800 4350 4800
Wire Wire Line
	4350 4800 4800 4800
Wire Wire Line
	4800 4800 4800 5000
Connection ~ 4350 4800
Wire Wire Line
	4800 5500 4800 5800
Wire Wire Line
	3850 5800 4350 5800
Connection ~ 4350 5800
Wire Wire Line
	3850 5200 3850 5400
Wire Wire Line
	3850 5200 3600 5200
Connection ~ 3850 5200
Wire Wire Line
	4350 5200 4350 5350
Connection ~ 4350 5350
Wire Wire Line
	4350 5350 4350 5400
$Comp
L Device:Fuse F1
U 1 1 606E5BEA
P 3450 5200
F 0 "F1" V 3253 5200 50  0000 C CNN
F 1 "Fuse" V 3344 5200 50  0000 C CNN
F 2 "Fuse:Fuse_0402_1005Metric_Pad0.77x0.64mm_HandSolder" V 3380 5200 50  0001 C CNN
F 3 "~" H 3450 5200 50  0001 C CNN
	1    3450 5200
	0    1    1    0   
$EndComp
$Comp
L pspice:DIODE D16
U 1 1 606F10FD
P 4050 2500
F 0 "D16" H 4000 2750 50  0000 L CNN
F 1 "DIODE" H 3950 2650 50  0000 L CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4050 2500 50  0001 C CNN
F 3 "~" H 4050 2500 50  0001 C CNN
	1    4050 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2500 3850 2500
Wire Wire Line
	4250 2500 4300 2500
$Comp
L Switch:SW_DPST_x2 SW1
U 1 1 606F4022
P 6650 5300
F 0 "SW1" H 6650 5535 50  0000 C CNN
F 1 "Maintenance" H 6650 5444 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_DIP_SPSTx01_Slide_6.7x4.1mm_W6.73mm_P2.54mm_LowProfile_JPin" H 6650 5300 50  0001 C CNN
F 3 "~" H 6650 5300 50  0001 C CNN
	1    6650 5300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4300 2500 4300 2950
$Comp
L pspice:CAP C2
U 1 1 6074A7D1
P 4050 3850
F 0 "C2" V 3735 3850 50  0000 C CNN
F 1 "10u" V 3826 3850 50  0000 C CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 4050 3850 50  0001 C CNN
F 3 "~" H 4050 3850 50  0001 C CNN
	1    4050 3850
	0    1    1    0   
$EndComp
$Comp
L pspice:DIODE D17
U 1 1 60749FE8
P 4100 3400
F 0 "D17" H 4100 3665 50  0000 C CNN
F 1 "DIODE" H 4100 3574 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4100 3400 50  0001 C CNN
F 3 "~" H 4100 3400 50  0001 C CNN
	1    4100 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 3850 3800 3400
Wire Wire Line
	3800 3400 3900 3400
Wire Wire Line
	4300 3850 4300 3400
Wire Wire Line
	5500 4800 4800 4800
Connection ~ 4800 4800
$Comp
L pspice:DIODE D15
U 1 1 60B94A1E
P 5950 4800
F 0 "D15" H 5950 5065 50  0000 C CNN
F 1 "DIODE" H 5950 4974 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5950 4800 50  0001 C CNN
F 3 "~" H 5950 4800 50  0001 C CNN
	1    5950 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 4800 5750 4800
Connection ~ 5500 4800
$Comp
L power:GND #PWR03
U 1 1 6076530A
P 6350 4100
F 0 "#PWR03" H 6350 3850 50  0001 C CNN
F 1 "GND" H 6355 3927 50  0000 C CNN
F 2 "" H 6350 4100 50  0001 C CNN
F 3 "" H 6350 4100 50  0001 C CNN
	1    6350 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D18
U 1 1 606F7BE4
P 6650 3750
F 0 "D18" V 6689 3633 50  0000 R CNN
F 1 "LED" V 6598 3633 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6650 3750 50  0001 C CNN
F 3 "~" H 6650 3750 50  0001 C CNN
	1    6650 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6350 3700 6350 4100
Wire Wire Line
	6650 3000 6650 3600
Wire Wire Line
	5650 3400 5600 3400
Wire Wire Line
	4800 5800 4350 5800
$Comp
L Device:R R9
U 1 1 6076A2BF
P 6650 2700
F 0 "R9" H 6720 2746 50  0000 L CNN
F 1 "560" H 6720 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric_Pad0.64x0.40mm_HandSolder" V 6580 2700 50  0001 C CNN
F 3 "~" H 6650 2700 50  0001 C CNN
	1    6650 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2850 6650 3000
Wire Wire Line
	6350 3000 6350 3100
Wire Wire Line
	6350 3000 6650 3000
$Comp
L TLV7011DCKT:TLV7011DCKT IC1
U 1 1 60BDED03
P 5600 3400
F 0 "IC1" H 6100 3665 50  0000 C CNN
F 1 "TLV7011DCKT" H 6100 3574 50  0000 C CNN
F 2 "lib:SOT65P210X110-5N" H 6450 3500 50  0001 L CNN
F 3 "http://www.ti.com/general/docs/lit/getliterature.tsp?genericPartNumber=TLV7011&fileType=pdf" H 6450 3400 50  0001 L CNN
F 4 "TEXAS INSTRUMENTS - TLV7011DCKT - COMPARATOR, MICROPOWER, SINGLE, SC-70-5" H 6450 3300 50  0001 L CNN "Description"
F 5 "1.1" H 6450 3200 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 6450 3100 50  0001 L CNN "Manufacturer_Name"
F 7 "TLV7011DCKT" H 6450 3000 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "595-TLV7011DCKT" H 6450 2900 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/TLV7011DCKT/?qs=F5EMLAvA7IAED6%2FatWfA3g%3D%3D" H 6450 2800 50  0001 L CNN "Mouser Price/Stock"
F 10 "TLV7011DCKT" H 6450 2700 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/tlv7011dckt/texas-instruments" H 6450 2600 50  0001 L CNN "Arrow Price/Stock"
	1    5600 3400
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 3400 4600 3400
Connection ~ 4300 3400
Wire Wire Line
	4300 2500 4500 2500
Wire Wire Line
	4500 3300 4600 3300
Connection ~ 4300 2500
Wire Wire Line
	5500 4150 5500 4800
Wire Wire Line
	6650 3900 6650 4800
Wire Wire Line
	6650 4800 6850 4800
Connection ~ 6650 4800
Wire Wire Line
	4500 2500 4500 3300
Wire Wire Line
	5600 3200 5600 2100
Wire Wire Line
	3150 2100 3150 4150
Wire Wire Line
	3200 5200 3300 5200
Wire Wire Line
	3200 5350 4350 5350
Text HLabel 3200 5200 0    50   Input ~ 0
Mains1
Text HLabel 3200 5350 0    50   Input ~ 0
Mains2
Connection ~ 4800 5800
Text GLabel 6850 5800 2    50   Input ~ 0
GND
$Comp
L 2N2222AUB:2N2222AUB Q1
U 1 1 60BCD97E
P 5950 3400
F 0 "Q1" H 6488 3446 50  0000 L CNN
F 1 "2N2222AUB" H 6488 3355 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 6500 3250 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 6500 3150 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 6500 3050 50  0001 L CNN "Description"
F 5 "2.16" H 6500 2950 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 6500 2850 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 6500 2750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 6500 2650 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 6500 2550 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 6500 2450 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 6500 2350 50  0001 L CNN "Arrow Price/Stock"
	1    5950 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2200 6650 2550
Text GLabel 6650 2200 2    50   Input ~ 0
VBattery
Wire Wire Line
	3650 2500 3800 2500
Connection ~ 3800 2500
Wire Wire Line
	3650 3400 3800 3400
Connection ~ 3800 3400
Wire Wire Line
	3800 2950 3800 2500
$Comp
L pspice:CAP C1
U 1 1 606F0621
P 4050 2950
F 0 "C1" V 4350 2900 50  0000 L CNN
F 1 "10u" V 4250 2900 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 4050 2950 50  0001 C CNN
F 3 "~" H 4050 2950 50  0001 C CNN
	1    4050 2950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6150 4800 6650 4800
Wire Wire Line
	6650 4800 6650 5100
Wire Wire Line
	6650 5500 6650 5800
Wire Wire Line
	4800 5800 6650 5800
Wire Wire Line
	6650 5800 6850 5800
Connection ~ 6650 5800
Text GLabel 6850 4800 2    50   Input ~ 0
InputVR
$Comp
L Device:R R10
U 1 1 60BAF3FE
P 5800 3400
F 0 "R10" V 5900 3400 50  0000 C CNN
F 1 "10" V 6000 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric_Pad0.64x0.40mm_HandSolder" V 5730 3400 50  0001 C CNN
F 3 "~" H 5800 3400 50  0001 C CNN
	1    5800 3400
	0    1    1    0   
$EndComp
Text GLabel 5700 3050 2    50   Input ~ 0
GND
Wire Wire Line
	5600 3300 5700 3300
Wire Wire Line
	5700 3300 5700 3050
Connection ~ 6650 3000
Text GLabel 3650 2500 0    50   Input ~ 0
GPIO3.3V
Text GLabel 3650 3400 0    50   Input ~ 0
GPIO3.3V
Wire Wire Line
	3150 2100 5600 2100
Wire Wire Line
	3150 4150 5500 4150
$EndSCHEMATC
